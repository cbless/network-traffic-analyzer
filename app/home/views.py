from flask import render_template
from flask_security import login_required, current_user


from . import home


@home.route('/', methods=['GET'])
@login_required
def index():
    return render_template('index.html')
