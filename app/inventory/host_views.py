from flask import render_template
from flask_security import  login_required

from ..core.models import Host

from . import inventory_bp
from flask import make_response

@inventory_bp.route('/hosts/', methods=['GET'])
@login_required
def host_list():
    hosts = Host.query.all()
    return render_template('host_list.html', hosts=hosts)


@inventory_bp.route('/hosts/export/csv', methods=['GET'])
@login_required
def host_list_export_csv():
    hosts = Host.query.all()
    host_list = ["MAC;IP;PRIVATE;HOSTNAME;OS"]
    for h in hosts:
        host_list.append("{0};{1};{2};{3};{4}".format(h.mac, h.ip, h.private, h.hostname, h.os))
    output = make_response("\n".join(host_list))
    output.headers["Content-Disposition"] = "attachment; filename=hosts.csv"
    output.headers["Content-type"] = "text/csv"
    return output


@inventory_bp.route('/hosts/<int:id>', methods=['GET'])
@login_required
def host_detail(id):
    host = Host.query.get_or_404(id)
    return render_template('host_details.html', host=host)

