from flask import render_template
from flask_security import login_required

from ..core.models import Connection

from . import inventory_bp
from flask import make_response


@inventory_bp.route('/connections/', methods=['GET'])
@login_required
def connection_list():
    con_list = Connection.query.all()
    return render_template('connection_list.html', connections=con_list)



@inventory_bp.route('/connections/export/csv', methods=['GET'])
@login_required
def connection_list_export_csv():
    connections = Connection.query.all()
    connection_list = ["SRC-MAC;SRC-IP;DST-MAC;DST-IP;TRANSPORT_LAYER;SRC-PORT;DST-PORT;LAYERS"]
    for c in connections:
        connection_list.append("{0};{1};{2};{3};{4};{5};{6};{7}".format(c.source.mac, c.source.ip,
                                                                        c.destination.mac, c.destination.ip,
                                                                        c.transport_layer, c.src_port, c.dst_port,
                                                                        c.layers))
    output = make_response("\n".join(connection_list))
    output.headers["Content-Disposition"] = "attachment; filename=connections.csv"
    output.headers["Content-type"] = "text/csv"
    return output
