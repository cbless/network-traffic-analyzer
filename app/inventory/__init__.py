from flask import Blueprint

inventory_bp = Blueprint('inventory', __name__, url_prefix='/inventory', template_folder='templates')

from .product_views import *
from .host_views import *
from .connection_views import *

