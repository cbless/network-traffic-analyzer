from flask import render_template
from flask_security import login_required
from sqlalchemy import func

from ..core.db import db
from ..core.models import ClientApplication, Service

from . import inventory_bp
from flask import make_response


@inventory_bp.route('/applications/', methods=['GET'])
@login_required
def application_list():
    applications = db.session.query(ClientApplication.name, func.count(ClientApplication.name)).group_by(
        ClientApplication.name).all()
    print(applications)
    return render_template('application_list.html', applications=applications)


@inventory_bp.route('/products/', methods=['GET'])
@login_required
def product_list():
    applications = db.session.query(ClientApplication.name, func.count(ClientApplication.name)).group_by(
        ClientApplication.name).all()
    services = db.session.query(Service.info, func.count(Service.info)).group_by(Service.info).all()
    return render_template('product_list.html', applications=applications, services=services)

