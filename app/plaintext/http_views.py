from flask import render_template, flash, redirect, url_for, request
from flask_security import current_user, login_required, roles_accepted

from ..core.models import Host, Service, DNSResponse, HTTPResource, Credential

from . import plaintext_proto_bp


@plaintext_proto_bp.route('/http/server/', methods=['GET'])
@login_required
def http_server_list():
    service = Service.query.filter(Service.protocol == 'HTTP')
    return render_template('server_list.html', service_name="HTTP",  service=service)


@plaintext_proto_bp.route('/http/resources/', methods=['GET'])
@login_required
def http_resources():
    resources = HTTPResource.query.all()
    return render_template('http_resources.html', resources=resources)


@plaintext_proto_bp.route('/http/resources/<int:id>', methods=['GET'])
@login_required
def http_resource_detail(id):
    resource = HTTPResource.query.get_or_404(id)
    return render_template('http_resource_detail.html', resource=resource)
