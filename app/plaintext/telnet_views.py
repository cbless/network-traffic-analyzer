from flask import render_template
from flask_security import login_required

from ..core.models import Service, DNSResponse

from . import plaintext_proto_bp


@plaintext_proto_bp.route('/telnet/server/', methods=['GET'])
@login_required
def telnet_server_list():
    service = Service.query.filter(Service.protocol == 'TELNET')
    return render_template('server_list.html', service_name="TELNET",  service=service)