from flask import render_template
from flask_security import login_required

from ..core.models import Service, DNSResponse

from . import plaintext_proto_bp


@plaintext_proto_bp.route('/dns/server/', methods=['GET'])
@login_required
def dns_server_list():
    service = Service.query.filter(Service.protocol == 'DNS')
    return render_template('server_list.html', service_name="DNS", service=service)


@plaintext_proto_bp.route('/dns/responses/', methods=['GET'])
@login_required
def dns_responses():
    resp = DNSResponse.query.all()
    return render_template('dns_response.html', response=resp)

