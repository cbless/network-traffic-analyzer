from flask import render_template
from flask_security import  login_required

from ..core.models import Service, Credential

from . import plaintext_proto_bp

@plaintext_proto_bp.route('/ftp/server/', methods=['GET'])
@login_required
def ftp_server_list():
    service = Service.query.filter(Service.protocol == 'FTP')
    return render_template('server_list.html', service_name="FTP", service=service)


@plaintext_proto_bp.route('/ftp/credentials/', methods=['GET'])
@login_required
def ftp_credentials():
    creds = Credential.query.filter(Credential.protocol == 'FTP')
    return render_template('ftp_creds.html', creds=creds)
