from flask import Blueprint

plaintext_proto_bp = Blueprint('plaintext', __name__, url_prefix='/plaintext', template_folder='templates')

from .dns_views import *
from .ftp_views import *
from .http_views import *
from .telnet_views import *

