from flask_script import Command, Option
from flask_security.utils import hash_password

from .db import db
from .models import Role, User
from .vars import ADMIN_ROLE_NAME, ASSESSOR_ROLE_NAME
from .plugins import PluginManager, PcapProcessor


class InitDB(Command):
    def run(self):
        admin_role = Role(name=ADMIN_ROLE_NAME, description='Privileged Role for administrative tasks.')
        db.session.add(admin_role)

        admin_user = User(email='admin@test.local', username='admin', password=hash_password('admin'),
                          active=True, roles=[admin_role])
        db.session.add(admin_user)

        assessor_role = Role(name=ASSESSOR_ROLE_NAME, description="Role for normal user access.")
        db.session.add(assessor_role)

        db.session.commit()


class ImportCommand(Command):

    option_list = (
        Option('--pcap', '-p', dest='filename'),
    )

    def run(self, filename):
        pm = PluginManager()
        pm.load_plugins()
        pm.process_pcap(filename, parse_per_plugin=False)

