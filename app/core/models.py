from .db import db

from flask_security import UserMixin, RoleMixin

# Define models
roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
        db.Column('role_id', db.Integer, db.ForeignKey('roles.id')))


class Role(db.Model, RoleMixin):
    __tablename__ = "roles"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.Text())

    # __str__ is required by Flask-Admin, so we can have human-readable values for the Role when editing a User.
    # If we were using Python 2.7, this would be __unicode__ instead.
    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return self.name

    # __hash__ is required to avoid the exception TypeError: unhashable type: 'Role' when saving a User
    def __hash__(self):
        return hash(self.name)


class User(db.Model, UserMixin):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    username = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    title = db.Column(db.String(10))
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    phone = db.Column(db.String(30))
    mobile = db.Column(db.String(30))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship(
        'Role',
        secondary=roles_users,
        backref=db.backref('users', lazy='dynamic')
    )

    def __str__(self):
        return "{0} {1}".format(self.first_name, self.last_name)

    def __repr__(self):
        return "{0} {1}".format(self.first_name, self.last_name)


class ClientApplication(db.Model):
    __tablename__ = "client_applications"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(1024), unique=False)
    host_id = db.Column(db.Integer, db.ForeignKey('hosts.id'), nullable=False)

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name


class Service(db.Model):
    __tablename__ = "services"
    id = db.Column(db.Integer, primary_key=True)
    transport_layer = db.Column(db.String(10), unique=False, nullable=False)
    protocol = db.Column(db.String(50), unique=False, nullable=True)
    port = db.Column(db.Integer(), nullable=False, unique=False)
    info = db.Column(db.Text(), unique=False, nullable=True)
    host_id = db.Column(db.Integer, db.ForeignKey('hosts.id'), nullable=False)

    def __repr__(self):
        return "{0}/{1}".format(self.transport_layer, str(self.port))

    def __str__(self):
        return "{0}/{1}".format(self.transport_layer, str(self.port))


class Host(db.Model):
    __tablename__ = "hosts"
    id = db.Column(db.Integer, primary_key=True)
    mac = db.Column(db.String(20), unique=False, nullable=True)
    ip = db.Column(db.String(50), unique=False)
    private = db.Column(db.Boolean(), default=True)
    hostname = db.Column(db.String(256), unique=False, nullable=True)
    os = db.Column(db.String(256),unique=False, nullable=True)
    applications = db.relationship('ClientApplication', backref='host', lazy='dynamic')
    services = db.relationship('Service', backref='host', lazy='dynamic')

    def __repr__(self):
        return self.ip

    def __str__(self):
        return self.ip


class Connection(db.Model):
    __tablename__ = "connections"
    id = db.Column(db.Integer, primary_key=True)

    src_id = db.Column(db.Integer, db.ForeignKey('hosts.id'))
    source = db.relationship("Host", foreign_keys=[src_id])

    dst_id = db.Column(db.Integer, db.ForeignKey('hosts.id'))
    destination = db.relationship("Host", foreign_keys=[dst_id])

    transport_layer = db.Column(db.String(10), unique=False, nullable=True)
    layers = db.Column(db.String(2048), unique=False, nullable=False)

    src_port = db.Column(db.Integer, unique=False, nullable=True)
    dst_port = db.Column(db.Integer, unique=False, nullable=True)



class HTTPResource(db.Model):
    __tablename__ = "http_resources"
    id = db.Column(db.Integer, primary_key=True)

    method = db.Column(db.String(20), unique=False, nullable=True)
    full_uri = db.Column(db.Text(), unique=False, nullable=True)
    uri = db.Column(db.Text(), unique=False, nullable=False)
    vhost = db.Column(db.String(2048), unique=False, nullable=False)

    client_id = db.Column(db.Integer, db.ForeignKey('hosts.id'))
    client = db.relationship("Host", foreign_keys=[client_id])

    server_id = db.Column(db.Integer, db.ForeignKey('hosts.id'))
    server = db.relationship("Host", backref="provided_http_resources", foreign_keys=[server_id])


class DNSResponse(db.Model):
    __tablename__ = "dns_response"
    id = db.Column(db.Integer, primary_key=True)
    qry = db.Column(db.String(1024), unique=False, nullable=False)

    server_id = db.Column(db.Integer, db.ForeignKey('hosts.id'))
    server = db.relationship("Host", backref="dns_response", foreign_keys=[server_id])


class ModbusRequest(db.Model):
    __tablename__ = "modbus_requests"
    id = db.Column(db.Integer, primary_key=True)

    master_id = db.Column(db.Integer, db.ForeignKey('hosts.id'))
    master = db.relationship("Host", backref="ModbusMaster", foreign_keys=[master_id])

    slave_id = db.Column(db.Integer, db.ForeignKey('hosts.id'))
    slave = db.relationship("Host", backref="ModbusSlave", foreign_keys=[slave_id])

    dstport = db.Column(db.Integer(), nullable=False, unique=False)
    func_code = db.Column(db.Integer, nullable=False, unique=False)
    func_name = db.Column(db.String(256), unique=False, nullable=True)

    def __repr__(self):
        return "{0} -> {1}:{2} ({3} - {4})".format(self.master, self.slave, self.dstport, self.func_code, self.func_name)

    def __str__(self):
        return "{0} -> {1}:{2} ({3} - {4})".format(self.master, self.slave, self.dstport, self.func_code, self.func_name)






class Credential(db.Model):
    __tablename__ = "credentials"
    id = db.Column(db.Integer, primary_key=True)

    client_id = db.Column(db.Integer, db.ForeignKey('hosts.id'))
    client = db.relationship("Host", foreign_keys=[client_id])

    server_id = db.Column(db.Integer, db.ForeignKey('hosts.id'))
    server = db.relationship("Host", foreign_keys=[server_id])

    protocol = db.Column(db.String(20), nullable=False, unique=False)

    dstport = db.Column(db.Integer(), nullable=False, unique=False)

    data = db.Column(db.String(1024), unique=False, nullable=True)

    def __repr__(self):
        return "{0} -> {1}:{2} ({3}/{4} - {5})".format(self.master, self.slave, self.protocol, self.dstport, self.data)

    def __str__(self):
        return "{0} -> {1}:{2} ({3}/{4} - {5})".format(self.master, self.slave, self.protocol, self.dstport, self.data)

