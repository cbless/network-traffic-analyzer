import pyshark
from straight.plugin import load

from .db import db
from .models import Host, Connection
from .utils import get_or_create, get_or_create_host

class PluginManager(object):

    def __init__(self, plugin_directory="plugins.pyshark"):
        self._plugin_directory = plugin_directory
        self._loaded_plugins = []
        self._plugins_by_layer = {}

    def load_plugins(self):
        # load plugins and create an Instanz for each of them
        plugins = load(self._plugin_directory, subclasses=PysharkPlugin)
        self._loaded_plugins = plugins.produce()
        for p in self._loaded_plugins:
            layer = p.layer
            if layer not in self._plugins_by_layer:
                self._plugins_by_layer[layer] = []
            self._plugins_by_layer[layer].append(p)
        import pprint
        pprint.pprint(self._plugins_by_layer)

    def plugins(self):
        return self._loaded_plugins

    def process_pcap(self, filename, parse_per_plugin=True):
        if parse_per_plugin:
            for p in self._loaded_plugins:
                processor = PcapProcessor(plugins=[p])
                processor.process_pcap(filename=filename)
        else:
            processor = PcapProcessor(plugins=self._loaded_plugins)
            processor.process_pcap(filename=filename)


class PysharkPlugin(object):

    def __init__(self, name, bpf_filter=None, display_filter=None, layer=None):
        self._name = name
        self._bpf_filter = bpf_filter
        self._display_filter = display_filter
        self._layer = layer

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def bpf_filter(self):
        return self._bpf_filter

    @bpf_filter.setter
    def bpf_filter(self, value):
        self._bpf_filter = value

    @property
    def display_filter(self):
        return self._display_filter

    @display_filter.setter
    def display_filter(self, value):
        self._display_filter = value

    @property
    def layer(self):
        return self._layer

    @layer.setter
    def layer(self, layer):
        self._layer = layer

    def _ensure_protocol(self, pkt):
        if self._layer in str(pkt.layers):
            return True
        else:
            return False

    def _handle_paket(self, pkt, src, dst):
        pass

    def process(self, pkt, src, dst):
        if not self._ensure_protocol(pkt=pkt):
            return
        self._handle_paket(pkt=pkt, src=src, dst=dst)

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class PcapProcessor(object):

    def __init__(self, plugins):
        self._plugins = plugins

    def _parse_with_plugins(self, pkt):
        try:
            src_mac = pkt.eth.src
            src_ip = pkt.ip.src
            dst_mac = pkt.eth.dst
            dst_ip = pkt.ip.dst

            src = get_or_create_host(db.session, mac=src_mac, ip=src_ip)
            dst = get_or_create_host(db.session, mac=dst_mac, ip=dst_ip)

            tl = None
            src_port = None
            dst_port = None
            layers = ",".join([x.layer_name for x in pkt.layers])
            try:
                src_port = pkt[pkt.transport_layer].srcport
                dst_port = pkt[pkt.transport_layer].dstport
                tl = pkt.transport_layer
                print("{0} - {1}:{2} -> {3}:{4}".format(tl, src, src_port, dst, dst_port))
            except:
                print("{0} - {1} -> {2}".format(pkt.highest_layer, src, dst))

            connection = get_or_create(db.session, Connection, src_id=src.id, dst_id=dst.id, transport_layer=tl,
                                       layers=layers, src_port=src_port, dst_port=dst_port)
            for p in self._plugins:
                p.process(pkt=pkt, src=src, dst=dst)
        except AttributeError as e:
            pass


    def process_pcap(self, filename):
        self._process_pcap_plugins(filename, self._plugins)

    def _process_pcap_plugins(self, filename, plugins):
        display_filter = []
        for p in plugins:
            df = p.display_filter
            if df not in display_filter:
                display_filter.append(df)
        print("[*] Starting plugins: {0}".format(str(plugins)))
        print("[*] Applying display filter: {0}".format(" || ".join(display_filter)))
        cap = pyshark.FileCapture(filename, display_filter=" || ".join(display_filter), only_summaries=False)
        #cap = pyshark.FileCapture(filename, only_summaries=False)
        cap.apply_on_packets(self._parse_with_plugins)
        print("[*] Done.")