from netaddr import IPAddress
from .models import Host, ClientApplication, Connection


def get_or_create(session, model, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()
    if not instance:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        session.refresh(instance)
        print("[+] '{0}' object created: {1}".format(model.__class__.__name__, instance))
    return instance


def get_or_create_host(session, mac, ip):
    instance = session.query(Host).filter_by(mac=mac, ip=ip).first()
    if not instance:
        is_private = IPAddress(ip).is_private()
        instance = Host(mac=mac, ip=ip, private=is_private)
        instance.private = is_private
        session.add(instance)
        session.commit()
        session.refresh(instance)
        print("[+] Host object created: {0}".format(instance))
    return instance