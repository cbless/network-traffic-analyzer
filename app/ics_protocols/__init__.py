from flask import Blueprint

ics_bp = Blueprint('ics', __name__, url_prefix='/ics', template_folder='templates')

from .views import *

