from flask import render_template, flash, redirect, url_for, request
from flask_security import current_user, login_required, roles_accepted

from ..core.models import Host, ModbusRequest
from ..core.db import db

from . import ics_bp


@ics_bp.route('/modbus/', methods=['GET'])
@login_required
def modbus_list():
    mb_requests = ModbusRequest.query.all()

    return render_template('modbus_list.html', mb_requests=mb_requests)


@ics_bp.route('/modbus/systems/', methods=['GET'])
@login_required
def modbus_masters_slaves():
    masters = db.session.query(Host).join(ModbusRequest, ModbusRequest.master_id == Host.id).all()
    slaves = db.session.query(Host).join(ModbusRequest, ModbusRequest.slave_id == Host.id).all()

    return render_template('modbus_master_slave.html', masters=masters, slaves=slaves)
