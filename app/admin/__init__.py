from flask import abort, redirect, url_for, request
from flask_admin import helpers as admin_helpers
from flask_admin import Admin
from flask_admin.base import MenuLink

from ..core.models import Role, User, ClientApplication, Host

from .views import *

# admin = Admin(name="Admin", base_template='base_layout_admin.html', template_mode='bootstrap3',)
admin = Admin(name="Admin", template_mode='bootstrap3',)


def configure_admin(app, db):
    admin.init_app(app)
    # Add model views
    admin.add_link(MenuLink(name='Back Home', url='/'))
    admin.add_view(AdminView(Role, db.session, category='Accounts'))
    admin.add_view(UserAdminView(User, db.session, category='Accounts'))
    admin.add_view(AdminView(Host, db.session, category='Inventory'))
    admin.add_view(AdminView(ClientApplication, db.session, category='Inventory'))

    @app.security.context_processor
    def security_context_processor():
        return dict(
            admin_base_template=admin.base_template,
            admin_view=admin.index_view,
            h=admin_helpers,
            get_url=url_for
        )

