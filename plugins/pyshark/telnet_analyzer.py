from app.core.plugins import PysharkPlugin

from app.core.db import db
from app.core.utils import get_or_create
from app.core.models import Service

class TelnetAnalyzer(PysharkPlugin):

    def __init__(self):
        PysharkPlugin.__init__(self, name="TelnetAnalyzer", layer="TELNET",
                               bpf_filter="tcp port 23",
                               display_filter="telnet.data")

    def _handle_paket(self, pkt, src, dst):
        try:
            sport = pkt[pkt.transport_layer].srcport
            dport = pkt[pkt.transport_layer].dstport

            if dport == str(23):
                service = get_or_create(db.session, Service, transport_layer=pkt.transport_layer, port=dport,
                                        protocol='TELNET', host_id=dst.id)
            elif sport == str(23):
                service = get_or_create(db.session, Service, transport_layer=pkt.transport_layer, port=sport,
                                        protocol='TELNET', host_id=src.id)
        except Exception as e:
            pass