from app.core.plugins import PysharkPlugin

from app.core.db import db
from app.core.utils import get_or_create
from app.core.models import ClientApplication, HTTPResource

class HttpRequestAnalyzer(PysharkPlugin):

    def __init__(self):
        PysharkPlugin.__init__(self, name="HttpRequestAnalyzer", layer="HTTP",
                               bpf_filter="tcp port 80 or tcp port 8080 or tcp port 8081",
                               display_filter="http.request")

    def _handle_paket(self, pkt, src, dst):
        try:
            method = pkt.http.request_method
            host = pkt.http.host
            uri = pkt.http.request_uri
            full_uri = pkt.http.request_full_uri
            uagent = pkt.http.user_agent

            # get existing product instance or create a new instance if no product can be found
            product = get_or_create(db.session, ClientApplication, name=uagent, host_id=src.id)

            # get existing HTTP Resource instance or create a new instance if no resource can be found
            http_resource=get_or_create(db.session, HTTPResource, method=method, uri=uri, full_uri=full_uri,
                                        vhost=host, client_id=src.id, server_id=dst.id)

            # update hostname if it is set in the response
            if dst.hostname is None:
                dst.hostname = host
                db.session.add(dst)
                db.session.commit()
                db.session.refresh(dst)
        except Exception as e:
            pass