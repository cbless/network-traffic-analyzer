from app.core.plugins import PysharkPlugin
from app.core.models import Service, Host, ClientApplication
from app.core.db import db
from app.core.utils import get_or_create

class HttpResponseAnalyzer(PysharkPlugin):

    def __init__(self):
        PysharkPlugin.__init__(self, name="HttpResponseAnalyzer", layer="HTTP",
                               bpf_filter="tcp port 80 or tcp port 8080 or tcp port 8081",
                               display_filter="http.response")

    def _handle_paket(self, pkt, src, dst):
        try:
            transport_layer = pkt.transport_layer
            src_port = pkt[pkt.transport_layer].srcport

            if "response_code" not in pkt.http.field_names:
                return

            server = ""
            try:
                server = pkt.http.server
            except:
                pass

            service = get_or_create(db.session, Service, transport_layer=transport_layer, port=src_port,
                                    protocol='HTTP', host_id=src.id)

            if len(server) > 0:
                service.info = server
                db.session.add(service)
                db.session.commit()
                db.session.refresh(service)

        except AttributeError as e:
            pass

