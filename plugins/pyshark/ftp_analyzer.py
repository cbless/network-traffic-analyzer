from app.core.plugins import PysharkPlugin

from app.core.db import db
from app.core.utils import get_or_create
from app.core.models import Credential, Service

class TelnetAnalyzer(PysharkPlugin):

    def __init__(self):
        PysharkPlugin.__init__(self, name="FTPAnalyzer", layer="FTP",
                               bpf_filter="tcp port 21",
                               display_filter="ftp")

    def _handle_paket(self, pkt, src, dst):
        try:
            sport = pkt[pkt.transport_layer].srcport
            dport = pkt[pkt.transport_layer].dstport

            if "request_command" in str(pkt.ftp.field_names):
                if "USER" in pkt.ftp.request_command:
                    get_or_create(db.session, Credential, client_id=src.id, server_id=dst.id, protocol="FTP",
                                  dstport=dport, data="USER {0}".format(pkt.ftp.request_arg))
                if "PASS" in pkt.ftp.request_command:
                    get_or_create(db.session, Credential, client_id=src.id, server_id=dst.id, protocol="FTP",
                                  dstport=dport, data="PASS {0}".format(pkt.ftp.request_arg))
                service = get_or_create(db.session, Service, transport_layer=pkt.transport_layer, port=dport,
                                        protocol='FTP', host_id=dst.id)
            if "response_code" in str(pkt.ftp.field_names):
                service = get_or_create(db.session, Service, transport_layer=pkt.transport_layer, port=sport,
                                        protocol='FTP', host_id=src.id)
        except Exception as e:
            pass