from app.core.plugins import PysharkPlugin
from app.core.models import Host, DNSResponse, Service
from app.core.db import db
from app.core.utils import get_or_create

class DNSResponseAnalyzer(PysharkPlugin):

    def __init__(self):
        PysharkPlugin.__init__(self,
                               name="DNSResponseAnalyzer", layer="DNS",
                               bpf_filter="tcp port 53 or udp port 53",
                               display_filter="(dns.qry.name && (udp.srcport==53 || tcp.srcport==53))")

    def _handle_paket(self, pkt, src, dst):
        try:
            if int(pkt[pkt.transport_layer].srcport) == 53:
                src_port = pkt[pkt.transport_layer].srcport
                dst_port = pkt[pkt.transport_layer].dstport

                qry_name = pkt.dns.qry_name

                resp = get_or_create(db.session, DNSResponse, qry=qry_name, server_id=src.id)
                service = get_or_create(db.session, Service, transport_layer=pkt.transport_layer, port=src_port,
                                        protocol='DNS', host_id=src.id)
        except AttributeError as e:
            pass