from app.core.plugins import PysharkPlugin
from app.core.models import Host, ModbusRequest, Service
from app.core.db import db
from app.core.utils import get_or_create

UNKNOWN_FUNC = "UNKNOWN"

func_names = {
    1 : "Read Coils",
    2 : "Read Discrete Inputs",
    3 : "Read Multiple Holding Registers",
    4 : "Read Input Registers",
    5 : "Write Single Coil",
    6 : "Write Single Holding Register",
    7 : "Read Exception Status",
    8 : "Diagnostic",
    9 : "",
    10: "",
    11: "Get Com Event Counter",
    12: "Get Com Event Log",
    13: "",
    14: "",
    15: "Write Multiple Coils",
    16: "Write Multiple Holding Registers",
    17: "Report Slave ID",
    18: "",
    19: "",
    20: "Read File Record",
    21: "Write File Record",
    22: "Mask Write Register",
    23: "Read/Write Multiple Registers",
    24: "Read FIFO Queue",
    25: "",
    43: "Read Device Identification"


}

class ModbusAnalyzer(PysharkPlugin):

    def __init__(self):
        PysharkPlugin.__init__(self,
                               name="ModbusAnalyzer", layer="MBTCP",
                               bpf_filter="tcp port 502",
                               display_filter="(mbtcp && tcp.dstport == 502)")

    def _handle_paket(self, pkt, src, dst):
        try:
            if int(pkt[pkt.transport_layer].dstport) == 502:
                src_port = pkt[pkt.transport_layer].srcport
                dst_port = pkt[pkt.transport_layer].dstport
                func_code = int(pkt.modbus.func_code)
                func_name = UNKNOWN_FUNC
                if func_code in func_names:
                    func_name = func_names[func_code]

                req = get_or_create(db.session, ModbusRequest, master_id=src.id, slave_id=dst.id, dstport=dst_port,
                                    func_code=func_code, func_name=func_name)
                service = get_or_create(db.session, Service, transport_layer=pkt.transport_layer, port=dst_port,
                                        protocol='MBTCP', host_id=dst.id)

        except AttributeError as e:
            pass