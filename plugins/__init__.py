import pyshark
from straight.plugin import load

loaded_plugins = []


class PacketProcessor(object):

    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    def process(self, pkt):
        pass


class PysharkPacketProcessor(PacketProcessor):

    def __init__(self, name, bpf_filter=None, display_filter=None):
        self._name = name
        self._bpf_filter = bpf_filter
        self._display_filter = display_filter

    @property
    def bpf_filter(self):
        return self._bpf_filter

    @bpf_filter.setter
    def bpf_filter(self, value):
        self._bpf_filter = value

    @property
    def display_filter(self):
        return self._display_filter

    @display_filter.setter
    def display_filter(self, value):
        self._display_filter = value




def load_pyshark_plugins():
    # load plugins and create an Instanz for each of them
    plugins = load('plugins.pyshark', subclasses=PacketProcessor)
    plugin_instances = plugins.produce()
    return plugin_instances


def parse_packet_summary_true(pkt):
    statistics_parser(pkt)


def run_all_summary_true(filename):
    cap = pyshark.FileCapture(filename, only_summaries=True)
    cap.apply_on_packets(parse_packet_summary_true)


def parse_with_plugins(pkt):
    for p in loaded_plugins:
        p.process(pkt)


def run_all_plugins_live(interface):
    global loaded_plugins
    loaded_plugins = load_pyshark_plugins()
    print(len(loaded_plugins))
    bpf_filter = []
    display_filter = []
    for p in loaded_plugins:
        bf = p.bpf_filter
        if bf not in bpf_filter:
            bpf_filter.append(bf)
        df = p.display_filter
        if df not in display_filter:
            display_filter.append(df)
    f = " OR ".join(bpf_filter)
    print (" || ".join(display_filter))

    cap = pyshark.LiveCapture(interface=interface, bpf_filter=f)



def run_all_plugins(filename):
    global loaded_plugins
    loaded_plugins = load_pyshark_plugins()
    print(len(loaded_plugins))
    bpf_filter = []
    display_filter = []
    for p in loaded_plugins:
        bf = p.bpf_filter
        if bf not in bpf_filter:
            bpf_filter.append(bf)
        df = p.display_filter
        if df not in display_filter:
            display_filter.append(df)
    print (" OR ".join(bpf_filter))
    print (" || ".join(display_filter))

    #cap = pyshark.FileCapture(filename, display_filter=display_filter, only_summaries=False)

